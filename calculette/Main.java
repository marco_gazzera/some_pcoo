import cal.*;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		/* On lis la ligne de texte tapée par l'utilisateur */
		System.out.println("Il s'agit d'une super calculatrice et elle peut tout résoudre ou presque!");

		Scanner sc = new Scanner(System.in);
		String line = sc.nextLine();

		Operation op = new Operation(line);
		System.out.println("= " + op.resolve());
		
		
		System.out.println("\nFin du calcul");
	}

}
