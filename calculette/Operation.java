package cal;

import java.util.ArrayList;

public class Operation {

	/* Exemple : [ 25.0 , + , 100.0 , / , 5 ], pour une opération correspondant au string 25+100/5 */
	private ArrayList OP; // Liste contenant l'opération, qui sera inchangée, sauf les parenthèses
	private ArrayList OP_Resolved; // Liste contenant l'opération qui se verra modifiée afin de la résoudre
	
	/* Opérateurs */
	private final char[] operateurs = { '+', '-', '*', '/' };
	private final char[] multiplicateurs = { '*', '/' };
	private final char[] additionneurs = { '+', '-' };
	
	/* On construit à partir d'une chaine de caractères */
	public Operation(String opera) {
		/* Initialisation des listes */
		this.OP = new ArrayList();
		this.OP_Resolved = new ArrayList();
		
		String operation = opera;
		
		/* On vérifie s'il y a des parenthèses, s'il y en a, on appelle la fonction qui traitera celle-ci comme
		une nouvelle opération, la résoudra, et remplacera les parenthèses et leur contenu par le résultat de l'opération. */
		operation = this.checkParentheses(operation);
		
		try {
			/* Tant qu'il y a un opérateur arithmétique dans l'opération */
			while (this.nextOperator(operation) > 0 ) {
				
				int pos = this.nextOperator(operation); // Position du premier opérateur dans le string
				String ns = operation.substring(0, pos); // On récupère tout ce qu'il y a avant, qui est forcément un nombre
				
				this.addNumber(Double.parseDouble(ns)); // Ajoute le nombre à la liste de l'opération
				this.addOperator(operation.charAt(pos)); // Ajoute l'opérateur
				
				operation = operation.substring(pos+1, operation.length()); // On enlève l'opérateur et le nombre qui le précédait du string
			}
			
			/* Il n'y a plus d'opérateur, mais il reste peut-être un nombre */
			if (operation.length() > 0) {
				this.OP.add(Double.parseDouble(operation));
			}
			
			this.OP_Resolved = this.OP; // On copie sur la deuxième liste également
			
		} catch(OperationException e) { // Un incident dans nos fonctions personnels
			System.out.println("il y a eu une erreur");
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	// Résous l'opération
	public double resolve() {
		this.doMultiplying(); // Priorité aux multiplications et divisions (Multiplication par l'inverse)
		this.doAddition(); // On effectue les additions et soustractions
		
		return (double) OP_Resolved.get(0); // Il ne reste plus que le resultat dans la liste, le reste a été supprimé
	}
	
	// Affiche toutes les valeurs du tableau OP
	public String toString() {
		return OP.toString();
	}
	
	// Ajoute un opérateur, si les conditions sont valides
	private void addOperator(char operator) throws OperationException {
		
		/* Si la liste n'est pas vide et qu'il y a bien déjà un nombre */
		if (!OP.isEmpty()) {
			if (OP.get(OP.size() - 1) instanceof Double) {

				this.OP.add(operator);
			}
		}
		else throw new OperationException();
		
		this.OP_Resolved = this.OP;
	}
	
	// Ajoute un nombre, si les conditons sont valides
	private void addNumber(double number) throws OperationException {
		
		/* Si la liste est vide ou que le dernier élément est un opérateur */
		if ( OP.isEmpty() || (OP.get(OP.size() - 1) instanceof Character )) {
			this.OP.add(number);
		}
		else throw new OperationException();
		
		this.OP_Resolved = this.OP;
	}
	
	// Cherche la première position du prochain opérateur arithmétique dans un string
	private int nextOperator(String s) {
		int min = -1;
		for (int i = 0; i<this.operateurs.length; i++) {
			int pos = s.indexOf(this.operateurs[i]);
			
			if (min == -1 && pos>=0 ) {
				min = pos;
			}
			if (pos>=0 && pos<min) { 
				min = pos; 
			}
 		}
		return min;
	}
	
	// Cherche la première position du prochain opérateur arithmétique dans une liste
	private int nextOperator(ArrayList s) {
		int min = -1;
		for (int i = 0; i<this.operateurs.length; i++) {
			int pos = s.indexOf(this.operateurs[i]);
			
			if (min == -1 && pos>=0 ) {
				min = pos;
			}
			if (pos>=0 && pos<min) { 
				min = pos; 
			}
 		}
		return min;
	}
		
	// Cherche la première position d'un opérateur arithmétique précis dans un string
	private int nextOperator(String s, char operator) {
		return s.indexOf(operator);
	}
	
	// Cherche la première position d'un opérateur arithmétique précis dans une liste
	private int nextOperator(ArrayList s, char operator) {
		return s.indexOf(operator);
	}
	
	// Cherche la première position du prochain multiplicateur arithmétique dans un string
	private int nextMultiplicator(String s) {
		int min = -1;
		for (int i = 0; i<this.multiplicateurs.length; i++) {
			int pos = s.indexOf(this.multiplicateurs[i]);
			
			if (min == -1 && pos>=0 ) {
				min = pos;
			}
			if (pos>=0 && pos<min) { 
				min = pos; 
			}
 		}
		return min;
	}
	
	// Cherche la première position du prochain multiplicateur arithmétique dans une liste
	private int nextMultiplicator(ArrayList s) {
		int min = -1;
		for (int i = 0; i<this.multiplicateurs.length; i++) {
			int pos = s.indexOf(this.multiplicateurs[i]);
			
			if (min == -1 && pos>=0 ) {
				min = pos;
			}
			if (pos>=0 && pos<min) { 
				min = pos; 
			}
 		}
		return min;
	}
	// Cherche la première position du prochain additionneur arithmétique dans un string
	private int nextAdder(String s) {
		int min = -1;
		for (int i = 0; i<this.additionneurs.length; i++) {
			int pos = s.indexOf(this.additionneurs[i]);
			
			if (min == -1 && pos>=0 ) {
				min = pos;
			}
			if (pos>=0 && pos<min) { 
				min = pos; 
			}
 		}
		return min;
	}
	
	// Cherche la première position du prochain additionneur arithmétique dans une liste
	private int nextAdder(ArrayList s) {
		int min = -1;
		for (int i = 0; i<this.additionneurs.length; i++) {
			int pos = s.indexOf(this.additionneurs[i]);
			
			if (min == -1 && pos>=0 ) {
				min = pos;
			}
			if (pos>=0 && pos<min) { 
				min = pos; 
			}
 		}
		return min;
	}

	/* S'il elles existent, récupère le contenu des parenthèses, en fait de nouvelles instances d'opération, et les remplace par
		résultat. Renvois le string de l'opération avec les parenthèses résolues
	*/
	private String checkParentheses(String str) {
		String s = str;
		while(s.indexOf('(') > -1) {
			int pos1 = s.indexOf('(');
			int pos2 = s.indexOf(')');
			
			String opSoustraite = s.substring(pos1+1, pos2); // Contenu, sans les parenthèses
			
			Operation op = new Operation(opSoustraite);
			double resultat = op.resolve();
			
			/* On remplace les parenthèses et le contenu par le résultat */
			StringBuilder sb = new StringBuilder();
			sb.append(s.substring(0, pos1));
			sb.append(resultat);
			sb.append(s.substring(pos2+1));
			
			s = sb.toString();
		}
		return s;
	}
	
	/* Supprime un caractère d'un string */
	private String charRemoveAt(String str, int p) {  
		return str.substring(0, p) + str.substring(p + 1);  
	} 
	
	/* Effectue toutes les multiplications de l'opération */
	private void doMultiplying() {		
		while (this.nextMultiplicator(this.OP_Resolved) > 0) {
			int pos = this.nextMultiplicator(this.OP_Resolved);
			
			double nbG = (double) this.OP_Resolved.get(pos-1);
			double nbD = (double) this.OP_Resolved.get(pos+1);
			char operator = (char) this.OP_Resolved.get(pos);
			
			double resultat = 0;	
			if (operator == '*') { 
				resultat = nbG * nbD; 
			}
			else if (operator == '/') { 
				resultat = nbG / nbD; 
			};
			
			this.OP_Resolved.set(pos, resultat);
			this.OP_Resolved.remove(pos-1);
			this.OP_Resolved.remove(pos);
		}
	}
	
	/* Effectue toute les additions de l'opération */
	private void doAddition() {		
		while (this.nextAdder(this.OP_Resolved) > 0) {
			int pos = this.nextAdder(this.OP_Resolved);
			
			double nbG = (double) this.OP_Resolved.get(pos-1);
			double nbD = (double) this.OP_Resolved.get(pos+1);
			char operator = (char) this.OP_Resolved.get(pos);
			
			double resultat = 0;	
			if (operator == '+') { 
				resultat = nbG + nbD; 
			}
			else if (operator == '-') { 
				resultat = nbG - nbD; 
			};
			
			this.OP_Resolved.set(pos, resultat);
			this.OP_Resolved.remove(pos-1);
			this.OP_Resolved.remove(pos);
		}
	}
}
