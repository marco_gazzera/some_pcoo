package genera;

public class Account<T>{
	// Attributs
	private String owner; 
	private double amount;
	private T currency;

	// Constructeur par défaut>
	public Account() {	
	}
	// Constructeur avec 3 arguments
	public Account(String owner, double amount, T currency) {
		this.owner = owner;
		this.amount = amount;
		this.currency = currency;
	}
	// Accesseurs
	public String getOwner() {
		return owner;
	}
	public double getAmount() {
		return amount;
	}
	public T getCurrency() {
		return currency;
	}
	// Modificateur
	public void setOwner (String owner) {
		this.owner = owner;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public void setCurrency(T currency) {
		this.currency = currency;
	}
	//Methodes
	public void addMoney(int amount) {
		this.amount += amount;
	}
	public void removeMoney(int amount) {
		this.amount -= amount;
	}
	public void showBalance(){
		System.out.println("Mr/Mme " + owner + " a " + amount + " en " + currency);
	}	
}