import genera.*;

public class Main {
	public static void main(String[] args) {
		Account<Character> account1 = new Account<>("Marco",2000.5,'$');
		account1.showBalance();
		Account<String> account2 = new Account<>("Pier",200.5,"Dollars");
		account2.showBalance();
		Bank bank = new Bank("Tradi");
		System.out.println("\n");
		bank.showBank();
		bank.transfert(account1, account2, 25);
		System.out.println("\n");
		account1.showBalance();
		account2.showBalance();
	}
}