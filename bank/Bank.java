package genera;

public class Bank{
	// Attributs
	private String name;
	// Constructeur par défaut
	public Bank(){
	}
	// Constructeur avec 1 arguement
	public Bank(String name){
		this.name = name;
	}
	// Accesseur
	public String getName(){
		return name;
	}
	//Methode
	public void showBank(){
		System.out.println("La bank " + name);
	}
	public <T, S> void transfert(Account<T> sourceAccount, Account<S> destinationAccount, int amount){
		if (sourceAccount.getAmount() >= amount){
			sourceAccount.removeMoney(amount);
			destinationAccount.addMoney(amount);
			System.out.println(sourceAccount.getOwner() + " a envoyer " + "" + amount + sourceAccount.getCurrency() + " à " + destinationAccount.getOwner());
		}else{
			System.err.println("Transaction impossible " + sourceAccount.getOwner() +" n' a pas assez de lovers");
		}
	}
}