import java.util.Random;
import java.util.Scanner;

public class Main {

    // creer le scanner pour toutes les fonctions
    private Scanner scanner = new Scanner(System.in);

    // rendre accessible un element pour savoir si le joueur à choisi une clef
    private boolean clef = false;

    // une methode pour le parfeux n°1
    public void parfeux1() {

        // remettre clef à faux
        clef = false;

        // afficher ce message dans la console
        System.out.println("Bienvenue devant le parfeux n°1");
        System.out.println("Quel est votre pseudo ?\n");

        // créer une variable pour recuperer le pseudo
        // met le code en attente pour que notre joueur puisse entrer son pseudo
        String pseudo = scanner.nextLine();

        // verifier si le pseudo est "MA2022"
        if(pseudo.equalsIgnoreCase("MA2022")){
            // afficher "Decryptage terminé"
            System.out.println("Decryptage terminé");

            // utiliser random de java
            Random random = new Random();
            // choisir un chiffre entre 1 et 9
            int nombreHasard = random.nextInt(9);

            // teleporter vers parfeux 2
            parfeux2();

        }
        else{
            System.out.println("Accès au server non autorisée !");
        }
    }

    // une methode pour le parfeux n°2
    public void parfeux2() {
        System.out.println("Bienvenue devant le parfeux n°2 !");

        System.out.println("Entrer un chiffre entre 1 et 9");

        int chiffre = scanner.nextInt();

        // condition pour verifier si le chiffre est entre 1 et 9
        if(chiffre > 0 && chiffre < 10){
            // verifier si le nombre était pair ou impair
            // si en divisant par 2 j'ai aucun reste c'est un nombre pair
            if(chiffre % 2 == 0){
                // nombre pair
                System.out.println("En cours de chargement");

                // rediriger vers parfeux 3
                parfeux3();
            }
            else{
                // nombre impair
                // rediriger vers parfeux 1 bis
                parfeux1bis();
            }

        }
        else{
            // une erreur
            parfeux1bis();
        }

    }

    // une methode pour le parfeux n°3
    public void parfeux3() {
        System.out.println("Bienvenue devant le parfeux n°3, en attente");

        // proposer d'entrer une phrase
        System.out.println("Entrer une phrase: ");

        Scanner scanner = new Scanner(System.in);

        // recolter la phrase en question
        String phrase = scanner.nextLine();

        // verifier si la phrase contient "marco"
        // lowercase pour mettre la phrase en minuscule
        if(phrase.toLowerCase().contains("marco")){
            // rediriger vers le parfeux 2 bis
            parfeux2bis();
        }
        else{
            // rediriger vers le parfeux 2
            parfeux2();
        }

    }

    // une methode pour le parfeux n°1 bis
    public void parfeux1bis() {
        // afficher ce message dans la console
        System.out.println("Bienvenue devant le parfeux n°1");
        System.out.println("Quel est votre pseudo ?\n");

        Scanner scanner = new Scanner(System.in);

        // créer une variable pour recuperer le pseudo
        // met le code en attente pour que notre joueur puisse entrer son pseudo
        String pseudo = scanner.nextLine();

        // verifier si le pseudo est "MA2022"
        if(pseudo.equalsIgnoreCase("MA2022")){
            System.out.println("L'erreur est la raison du doute");
            // redirige vers le parfeux 1
            parfeux1();
        }
        else{

            // proposer un clef
            System.out.println("Souhaites-tu un clef ? oui, non");

            // recolter sa réponse
            String reponse = scanner.nextLine();

            if(reponse.equalsIgnoreCase("oui")){
                // ajouter
                System.out.println("Super :)");
                // mettre clef sur vrai
                clef = true;
                // rediriger vers parfeux 2
                parfeux2();
            }
            else{
                // on le rajoute pas
                System.out.println("Dommage...");
                parfeux4();
            }

        }
    }

    // une methode pour le parfeux n°2 bis
    public void parfeux2bis() {
        System.out.println("Bienvenue devant le parfeux n°2 ");

        // verifier si le joueur à choisit l'exilir -> rediriger vers le parfeux 4
        if(clef){
            System.out.println("La solution donne gain de cause");
            parfeux4();
        }

        // sinon parfeux 2
        else{
            parfeux2();
        }

    }

    // une methode pour le parfeux n°4
    public void parfeux4() {
        System.out.println("Bienvenue dans le parfeux n°4");

        // demander au joueur d'entrer un nombre ?
        System.out.println("Entrer un chiffre entre 1 et 9");

        int chiffre = scanner.nextInt();

        // condition pour verifier si le chiffre est entre 1 et 9
        if(chiffre > 0 && chiffre < 10){
            // verifier si le nombre était pair ou impair
            // si en divisant par 2 j'ai aucun reste c'est un nombre pair
            if(chiffre % 2 == 0){
                // nombre pair
                System.out.println("Gagné ! Tu as réussis toute les étapes tu es dans le server car tu es le vrai !");
            }
            else{
                parfeux1bis();
            }

        }
        else{
            // une erreur
            parfeux1bis();
        }

    }

    public static void main(String[] args) {

        // on créer un objet Main
        Main jeu = new Main();
        System.out.println("* Simulation connexion server *");
        // appeler le parfeux n°1
        jeu.parfeux1();


    }

}