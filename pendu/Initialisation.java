package initialisation;
import java.io.*;
import java.util.*;


public class Initialisation{

	public String motSecret(){

            String mot;
            do {
                System.out.println(" * Petit jeu du pendu * ");
                System.out.print("Quel est votre mot : ");
                Scanner saisie = new Scanner(System.in);
                mot = saisie.nextLine();
            } while (mot.length() == 0);

            return mot;
    }

    public String Init(String mot){
        int lg_mot = mot.length();
        String str = "";
        System.out.print("\033\143");
        System.out.println("Vous avez 11 essais");
        System.out.println("Le mot fait " + lg_mot + " lettres : ");
        for (int tiret = 1; tiret <= lg_mot; tiret++) {
            str+="- ";
        }
        return str;
    }

}