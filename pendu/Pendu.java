import initialisation.*;

import java.io.*;
import java.util.*;

public class Pendu extends Initialisation{

    public static void main(String[] args) {

        Pendu penduu = new Pendu();
        String mot = "";
        mot =  penduu.motSecret();
        int lg_mot = mot.length();


        String affichage = "";
        affichage = penduu.Init(mot);

        boolean valide;
        int nb_lettre_trouver;
        int nb_essai = 0;
        String lettre_deja_saisie = "";
        do {
            nb_lettre_trouver = 0;

            String lettre_tapee;
            do {
                System.out.println();
                System.out.println("Quelle est votre lettre : ");
                Scanner lettre = new Scanner(System.in);
                lettre_tapee = lettre.nextLine();
            }while(lettre_tapee.length() == 0);

            char lettre_seule = lettre_tapee.charAt(0);

            boolean deja_saisie = false;
            for(int ctp = 0; ctp < lettre_deja_saisie.length(); ctp++) {
                char une_lettre = lettre_deja_saisie.charAt(ctp);
                if(une_lettre == lettre_seule)    {
                    deja_saisie = true;
                }
            }
            
            if(deja_saisie)  {
                System.out.println("Non la lettre est déjà saisie");
            }else {
                lettre_deja_saisie += lettre_seule;
            }

            valide = false;
            for (int parcours = 0; parcours <= lg_mot - 1; parcours++) {
                if (lettre_seule == mot.charAt(parcours)) {
                    valide = true;
                }

                char lettre_a_afficher = '_';
                for(int ctp = 0; ctp < lettre_deja_saisie.length(); ctp++) {
                    char une_lettre = lettre_deja_saisie.charAt(ctp);
                    if(une_lettre == mot.charAt(parcours))   {
                        lettre_a_afficher = une_lettre;
                        nb_lettre_trouver++;
                    }
                }
                System.out.print(lettre_a_afficher + " ");

            }
            System.out.println();
            if (!valide) {
                System.out.println("La lettre n'est pas dans le mot !");
                nb_essai++;
            }
        }while(nb_lettre_trouver != mot.length() && nb_essai <= 11);

        if(nb_essai <= 11) {
            System.out.println("Merci d'avoir jouer :)");
            System.out.println("Le mot était bien : " + mot);
        }else if (nb_essai > 11){
            System.out.println("Perdu :(");
            System.out.println("Le mot était: " + mot);

        }

    }
}