import java.util.*;
import com.*;

public class Main{
	public static void main(String[] args){
		List<Toy> jouets = new ArrayList<>();
		jouets.add(new Toy("Loup garoux",15,"jeux cartes", 5));
		jouets.add(new Toy("Zebre",6,"figurine zebre", 10));
		jouets.add(new Toy("Araignée peluche",30,"doudou",5));
		jouets.add(new Toy("Truc",100,"inutile",5));
		jouets.add(new Toy("Lego",100,"construire",10));
		// tri par ordre alphabetique
		Collections.sort(jouets, new PriceComparator());
		// Afficher tous les noms
		for(Toy jouet  : jouets){
			System.out.println(jouet.getName()+ " " + jouet.getPrice()+ "$ " + jouet.getDescription()+ "à une promotion de " + jouet.getPromo() + " ");
		}
	}	
}