package com;
import java.util.*;

public class PriceComparator implements Comparator<Toy> {
	
	@Override
	public int compare(Toy t1,Toy t2) {
		// CROISSANT NIVEAU ALPHA 
		if (t1.getPrice() == t2.getPrice()) {
			return t1.getName().compareTo(t2.getName());
		}
		// PLUS INTERESSANT NIVEAU PROMO
		if (t1.getPrice() == t2.getPrice()) {
			return t1.compareTo(t2);
		}
		return t1.getPrice() - t2.getPrice();
	}
}