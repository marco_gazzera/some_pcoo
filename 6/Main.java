import viru.*;

public class Main {
	public static void main(String[] args) {
		for (Virus maladie : Virus.values()){
			System.out.println("La maladie: " + maladie.name() + " est une maladie " + maladie.isMortal() + 
				" description " + maladie.getDescription());
		}
	}
}