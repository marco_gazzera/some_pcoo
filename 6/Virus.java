package viru;

public enum Virus{	
	EBOLA(true, "made in afrique"),
	SIDA(true, "made in année 90"),
	CORONA(true, "made in china"),
	TATA(false, "pas juste"),
	TOTO(false, "pas juste");

	private boolean mortal;
	private String description;
	
	Virus (boolean mortal, String description){
		this.mortal = mortal;
		this.description = description;
	}
	public boolean isMortal(){
		return mortal;
	}
	public String getDescription(){
		return description;
	}
}