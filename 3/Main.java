import java.util.ArrayList;
import java.util.Collections;

public class Main {
    public static void main(String[] args) {
        int deckSize = 52;
        ArrayList<Integer> list = new ArrayList<>();
        for (int i = 0; i < deckSize; ++i) {
            list.add(i);
        }
        System.out.println("Simulation d'un mélange d'un jeu de carte de 52 cartes \n");
        System.out.println("Liste de base: " + list);
        Collections.shuffle(list);
        System.out.println("\n");
        System.out.println("Liste modifiée: " + list);
    }
}